/*
 * Sample program for DokodeModem
 * GPIO　サンプル
 * Copyright (c) 2024 Circuit Desgin,Inc
 * Released under the MIT license
 */
#include <dokodemo.h>

DOKODEMO Dm = DOKODEMO();

void setup()
{
  Dm.begin(); // 初期化が必要です。

  Dm.LedCtrl(RED_LED, OFF);
  Dm.LedCtrl(GREEN_LED, OFF);
}

void loop()
{
  // 外部端子がHIGHだったら赤LEDを点灯します
  if (Dm.readExIN())
  {
    Dm.LedCtrl(RED_LED, ON);
  }

  Dm.LedCtrl(GREEN_LED, ON); // 特に意味はありませんが、わかりやすくするために点灯します
  Dm.exOutCtrl(ON);          // OUT端子をONします
  delay(1000);

  Dm.LedCtrl(GREEN_LED, OFF); // 特に意味はありませんが、わかりやすくするために消灯します
  Dm.exOutCtrl(OFF);          // OUT端子をOFFします
  delay(1000);
}
